const Discord = require('discord.js');
const config = require("./config.json");

const client = new Discord.Client();
client.prefix = config.prefix;

client.on("ready",() => {
    console.log("Bot Iniciado!");
    console.log("Users: "+client.users.size+"\n Servidores: " + client.guilds.size );
    client.user.setActivity(` ${client.users.size} users`, {type:"Watching"})
});

client.on("message", async message => {
    let msg = message.content.toLowerCase();
    if(message.author.bot){ return undefined; }
    if(message.content.indexOf(client.prefix) !== 0){ return; }
    const args = msg.slice(client.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLocaleLowerCase();

    try{
        let commands = require(`./commands/${command}.js`);
        commands.run(client,message,args);
    }catch(e){
        console.log(e);
    }finally{

    }
})


client.login(config.token);
