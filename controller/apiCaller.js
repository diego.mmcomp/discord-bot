Axios = require("axios");

exports.getPlayer = (nickName) => {
    return Axios.get(`https://gameinfo.albiononline.com/api/gameinfo/search?q=${nickName}`).then(response => response.data);
}
