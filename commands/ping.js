exports.run = async (client,message,args) => {
    const m = await message.channel.send("Calculando...");

    m.edit(`Pong! :ping_pong:
    Latencia do Bot: ${m.createdTimestamp - message.createdTimestamp}ms
    Latencia da API: ${Math.round(client.ping)}ms`);
}